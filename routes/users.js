const express = require('express');
const router = express.Router();

const pool = require('../db/db');

/**
 * @swagger
 * /users:
 *   get:
 *     summary: Get all users
 *     responses:
 *       200:
 *         description: Successful response
 *     tags:
 *         - Users
 */
router.get('/users', async (req, res) => {
    try {
        const result = await pool.query('SELECT * FROM users');
        res.json(result.rows);
    } catch (error) {
        console.error('Error executing query', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});

/**
 * @swagger
 * /users:
 *   post:
 *     summary: Create a new user
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               email:
 *                 type: string
 *               password:
 *                 type: string
 *               telnumber:
 *                 type: string
 *               name:
 *                 type: string
 *               surname:
 *                 type: string
 *               firstname:
 *                 type: string
 *               address:
 *                 type: string
 *               localization:
 *                 type: string
 *               pictures:
 *                 type: integer
 *     responses:
 *       201:
 *         description: User created successfully
 *       400:
 *         description: Bad request
 *       500:
 *         description: Internal Server Error
 *     tags:
 *       - Users
 */
router.post('/users', async (req, res) => {
    try {
        const {
            email,
            password,
            telnumber,
            name,
            surname,
            firstname,
            address,
            localization,
            pictures,
        } = req.body;

        // Perform input validation here if needed

        const result = await pool.query(
            'INSERT INTO users (email, password, telnumber, name, surname, firstname, address, localization, pictures) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) RETURNING *',
            [email, password, telnumber, name, surname, firstname, address, localization, pictures]
        );

        res.status(201).json(result.rows[0]);
    } catch (error) {
        console.error('Error executing query', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});

module.exports = router;