/**
 * @swagger
 * /pictures:
 *   get:
 *     summary: Get all pictures
 *     responses:
 *       200:
 *         description: Successful response
 *     tags:
 *         - Pictures
 */
const express = require('express');
const router = express.Router();

const pool = require('../db/db');


/**
 * @swagger
 * /pictures:
 *   get:
 *     summary: Get all pictures
 *     responses:
 *       200:
 *         description: Successful response
 *     tags:
 *         - Pictures
 */
router.get('/pictures', async (req, res) => {
    try {
        return 'toto';
        // const result = await pool.query('SELECT * FROM pictures');
        // res.json(result.rows);
    } catch (error) {
        console.error('Error executing query', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});

/**
* @swagger
* /pictures:
*   post:
*     summary: Upload a new picture
*     requestBody:
*       required: true
*       content:
*         application/json:
*           schema:
*             type: object
*             properties:
*               image:
*                 type: string
*               forusers:
*                 type: boolean
*     responses:
*       201:
*         description: Picture uploaded successfully
*       400:
*         description: Bad request
*       500:
*         description: Internal Server Error
*     tags:
*       - Pictures
*/
router.post('/pictures', async (req, res) => {
    console.log(req, res);
    try {
        const { image, forusers } = req.body;

        // Perform input validation here if needed

        const result = await pool.query(
            'INSERT INTO pictures (image, forusers) VALUES ($1, $2) RETURNING *',
            [image, forusers]
        );

        res.status(201).json(result.rows[0]);
    } catch (error) {
        console.error('Error executing query', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});

module.exports = router;