const express = require('express');
const swaggerJsdoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const userRoute = require('./routes/users');
const picturesRoute = require('./routes/pictures');

const app = express();
app.use(express.json());

const port = 3000;

// Swagger configuration
const options = {
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'Express Swagger API',
            version: '1.0.0',
            description: 'A sample API with Swagger documentation',
        },
    },
    apis: ['./routes/*.js'],
};
const specs = swaggerJsdoc(options);

// Middleware to serve Swagger UI
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(specs));

app.use('/users', userRoute);
app.use('/pictures', picturesRoute);

// Start the server
app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
